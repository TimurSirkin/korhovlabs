#include "/usr/include/mpich/mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void createMessage(char *message, int messageSize)
{
	if (message != NULL)
	{
		free(message);
	}
  message = malloc(messageSize * sizeof(char));
}

int main(int argc, char *argv[])
{
  // MPI default variables
  int procId, procCount, nameLenght;
  char procName[MPI_MAX_PROCESSOR_NAME];

  // Message to be sended
  char *message = NULL;

  // Messages sizes in bytes
  int const messageSizesCount = 8;
  int messageSizes[8] = {8, 16, 32, 64, 128, 256, 512, 1024};

  // Message count which should be sended by each process
  int messageCount = 10;

  // MPI initialization
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &procCount);
  MPI_Comm_rank(MPI_COMM_WORLD, &procId);
  MPI_Get_processor_name(procName, &nameLenght);
  fprintf(stderr, "Process %d on %s\n", procId, procName);

  // For current lab process count shoulb be not less than 2
  if (procCount < 2)
  {
    if (procId == 0)
    {
      fprintf(stderr, "Process count should be not less than 2, current count = %d\n", procCount);
    }
    return 0;
  }

  double s_time;
  double e_time;

  s_time = MPI_Wtime();

  // For each message size value
  for (int i = 0; i < messageSizesCount; ++i)
  {
	  printf("Message creation on proc %d...\n", procId);
    createMessage(message, messageSizes[i]);
	  printf("Message created on proc %d\n", procId);

    int currentMessageCount = 0;
    while (currentMessageCount < messageCount)
    {
      MPI_Status Status;
      int destId = 0;
      if (procId == 0)
      {
        destId = 1;
      }
      MPI_Sendrecv(message, 1, MPI_CHAR, destId, MPI_ANY_TAG , message, 1, MPI_CHAR, destId, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
      
      ++currentMessageCount;
    }

    if (procId == 0)
    {
      e_time = MPI_Wtime();
      printf("Ellapsed time for message with %d bytes size is %f \n", messageSizes[i], e_time-s_time);
    }
  }

  MPI_Finalize();

  return 0;
}
