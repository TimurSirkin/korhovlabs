#include "/usr/include/mpich/mpi.h"
#include <stdio.h>
#include <math.h>

static void initArray(int *array, int arraySize, int maxInitValue)
{
  // Fill array
  for (int i = 0; i < arraySize; i++)
  {
    array[i] = i % maxInitValue;
  }
}

int main(int argc, char *argv[])
{
  int currentId, procCount, nameLenght;
  double startwtime = 0.0, endwtime;
  char procName[MPI_MAX_PROCESSOR_NAME];
  int arraySize = 500000;
  int maxInitValue = 90;
  int array[arraySize];

  int minValue;
  int maxValue;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &procCount);
  MPI_Comm_rank(MPI_COMM_WORLD, &currentId);
  MPI_Get_processor_name(procName, &nameLenght);

  fprintf(stderr, "Process %d on %s\n", currentId, procName);

  if (currentId == 0)
  {
    initArray(array, arraySize, maxInitValue);
  }

  double sp_time = MPI_Wtime();

  MPI_Bcast(array, arraySize, MPI_INT, 0, MPI_COMM_WORLD);

  // Get calculation edges
  int chunkSize = arraySize / procCount;
  int from = chunkSize * currentId;
  int to = chunkSize * (currentId + 1);
  if (currentId == procCount - 1)
  {
    to = arraySize;
  }

  // Init min and max vules
  int currentMin = maxInitValue;
  int currentMax = 0;

  for (int i = from; i < to; ++i)
  {
    // Check max value
    if (array[i] > currentMax)
    {
      currentMax = array[i];
    }

    // Check min value
    if (array[i] < currentMin)
    {
      currentMin = array[i];
    }
  }

  printf("Maximum value from process %d is %d.\n", currentId, currentMax);
  printf("Minimum value from process %d is %d.\n", currentId, currentMin);

  MPI_Reduce(&currentMax, &maxValue, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
  MPI_Reduce(&currentMin, &minValue, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);

  double ep_time = MPI_Wtime();

  MPI_Finalize();

  if (currentId == 0)
  {
    printf("Total max value = %d\n", maxValue);
    printf("Total min value = %d\n", minValue);
    printf("Time ellapsed: %f\n", ep_time - sp_time);
  }

  return 0;
}